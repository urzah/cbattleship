using System;

namespace Battleship
{ //start namespace
    class Map
    { //start Map Class
        // This class handles the drawing of the Map to the console, it accepts a 2-dimensional array
        // representing the current state of the game pieces.

        //private properties and methods ================================================
        private char[,] _playerSearch = new char[10,10];
        private char[,] _playerShips = new char[10,10];
        private char[,] _opponentSearch = new char[10,10];
        private char[,] _opponentShips = new char[10,10];


        //public properties and methods =================================================
        public Map()
        { //Start Map constructor
        } //End Map constructor
        public void Display()
        { //start Display Function
            //We need to add the box drawing characters to the info in the data
            //arrays before sending to the console.  We only need to worry about
            //showing the players search and ships as oppents stuff will always
            //be hidden.
            char[,] _playerSearchDisplay = new char[,]
                {//start array
                {'╔','═','═','═','═','═','═','═','═','═','═','═','╗'},
                {'║',' ',' ',' ','S','E','A','R','C','H',' ',' ','║'},
                {'╟','─','─','─','─','─','─','─','─','─','─','─','╢'},
                {'║',' ','A','B','C','D','E','F','G','H','I','J','║'},
                {'║','0','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','1','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','2','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','3','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','4','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','5','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','6','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','7','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','8','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','9','░','░','░','░','░','░','░','░','░','░','║'},
                {'╚','═','═','═','═','═','═','═','═','═','═','═','╝'}
                };//end array

            char[,] _playerShipsDisplay = new char[,]
                {//start array
                {'╔','═','═','═','═','═','═','═','═','═','═','═','╗'},
                {'║',' ',' ',' ','S','H','I','P','S',' ',' ',' ','║'},
                {'╟','─','─','─','─','─','─','─','─','─','─','─','╢'},
                {'║',' ','A','B','C','D','E','F','G','H','I','J','║'},
                {'║','0','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','1','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','2','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','3','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','4','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','5','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','6','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','7','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','8','░','░','░','░','░','░','░','░','░','░','║'},
                {'║','9','░','░','░','░','░','░','░','░','░','░','║'},
                {'╚','═','═','═','═','═','═','═','═','═','═','═','╝'}
                };//end array

            //add player Search info into search array
            for(int row = 0;row < 10; row++)
            {
                for(int col = 0; col < 10; col++)
                {
                    _playerSearchDisplay[row+4,col+2] = (_playerSearch[row,col] == '\0') ? '░' : _playerSearch[row,col];
                }
            }
            //add player Ships info ships into array
            for(int row = 0;row < 10; row++)
            {
                for(int col = 0; col < 10; col++)
                {
                    _playerShipsDisplay[row+4,col+2] = (_playerShips[row,col] == '\0') ? '░' : _playerShips[row,col];
                }
            }
        
            Console.Clear();
            for (int row = 0; row < _playerSearchDisplay.GetLength(0); row++)
            {
                for (int col = 0; col < _playerSearchDisplay.GetLength(1); col++)
                {
                    System.Console.Write(_playerSearchDisplay[row,col]);
                }
                System.Console.Write('\n');
            }

            for (int row = 0; row < _playerShipsDisplay.GetLength(0); row++)
            {
                for (int col = 0; col < _playerShipsDisplay.GetLength(1); col++)
                {
                    System.Console.Write(_playerShipsDisplay[row,col]);
                }
                System.Console.Write('\n');
            }
        } //end draw

        public void Addship(int row, int col, string dir, string type)
        { //start addship

            // row = Convert.ToInt32(Console.ReadLine());
            // column = Console.ReadLine();
            // direction = Console.ReadLine();
            // type = Console.ReadLine();
            // int col = Int32.Parse(column);
            // // int dir = Int32.Parse(direction);
            // _playerShips[row,col] = mapChar;

            if(type == "CV" || type == "cv")
            {
                for(int i = 0; i < 5; i++)
                {
                    if(dir == "h")
                    {
                        _playerShips[row,col+i] = 'A';
                    }
                    else
                    {
                    _playerShips[row+i,col] = 'A';
                    }
                }
            }

            if(type == "BB" || type == "bb")
            {
                for(int i = 0; i < 4; i++)
                {
                    if(dir == "h")
                    {
                        _playerShips[row,col+i] = 'B';
                    }
                    else
                    {
                        _playerShips[row+i,col] = 'B';
                    }
                }
            }

            if(type == "SS" || type == "ss")
            {
                for(int i = 0; i < 3; i++)
                {
                    if(dir == "h") 
                    {
                        _playerShips[row,col+i] = 'S';
                    }
                    else
                    {
                        _playerShips[row+i,col] = 'S';
                    }
                }
            }

            if(type == "CA" || type == "ca")
            {
                for(int i = 0; i < 3; i++)
                {
                    if(dir == "h")
                    {
                        _playerShips[row,col+i] = 'C';
                    }
                    else
                    {
                        _playerShips[row+i,col] = 'C';
                    }
                }
            }

            if(type == "DD" || type == "dd")
            {
                for(int i = 0; i < 2; i++)
                {
                    if(dir == "h")
                    {
                        _playerShips[row,col+i] = 'D';
                    }
                    else
                    {
                    _playerShips[row+i,col] = 'D';
                    }
                }
            } 
        } //end Add Ship
    } //End Map Class
} //end namespace